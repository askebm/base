inherit c make

SRC_URI = "https://downloads.sourceforge.net/project/linuxptp/v${SRC_MAINREV}/linuxptp-${SRC_REV}.tgz"

S = "${SRCDIR}/linuxptp-${SRC_REV}"


COMPATIBLE_HOST_ARCHS = ".*linux"

EXTRA_OEMAKE += "CROSS_COMPILE=${TARGET_PREFIX} prefix=${prefix} mandir=${mandir}"

DEPENDS = "librt libm"

inherit auto-package-utils
AUTO_PACKAGE_UTILS = "phc_ctl pmc hwstamp_ctl timemaster ptp4l phc2sys nsm"
AUTO_PACKAGE_UTILS_DEPENDS = "librt libm"
AUTO_PACKAGE_UTILS_RDEPENDS = "librt libm"

RDEPENDS_${PN} = "${AUTO_PACKAGE_UTILS_PROVIDES}"
