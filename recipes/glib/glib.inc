# -*- mode:python; -*-
require glib-common.inc

RECIPE_TYPES = "machine sdk"

SRC_URI += "file://config.site"
SRC_HOST_SITEFILES = "${SRCDIR}/config.site"

RECIPE_FLAGS += "glib_iconv"
DEPENDS += "${DEPENDS_ICONV}"
DEPENDS_ICONV:USE_glib_iconv = "libiconv"

RECIPE_FLAGS += "glib_threads"
DEPENDS += "${DEPENDS_THREADS}"
DEPENDS_THREADS_TYPE = "libpthread"
DEPENDS_THREADS_TYPE:HOST_OS_mingw32 = ""
DEPENDS_THREADS:USE_glib_threads = "${DEPENDS_THREADS_TYPE}"

DEPENDS += "${DEPENDS_PCRE}"
DEPENDS_PCRE += "libpcre2-8"
